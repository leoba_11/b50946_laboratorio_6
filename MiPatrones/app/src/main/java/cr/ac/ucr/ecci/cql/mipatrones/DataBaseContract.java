package cr.ac.ucr.ecci.cql.mipatrones;

import android.provider.BaseColumns;

public class DataBaseContract {

    // Para asegurar que no se instancie la clase hacemos el constructor privado
    private DataBaseContract() {
    }

    // Definimos una clase interna que define las tablas y columnas implementa la
    // interfaz BaseColumns para heredar campos estandar del marco de Android _ID
    public static class DataBaseEntry implements BaseColumns {

        // Clase TableTop
        public static final String TABLE_NAME_PERSONA = "Persona";


        // private String Nombre;
        public static final String COLUMN_NAME_NOMBRE = "Nombre";
        // private String IdImagen;
        public static final String COLUMN_NAME_IDIMAGEN = "IdImagen";


    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    // Creacion de tabla Persona
    public static final String SQL_CREATE_PERSONA =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSONA + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_IDIMAGEN + TEXT_TYPE + " )";

    public static final String SQL_DELETE_PERSONA =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSONA;

}
