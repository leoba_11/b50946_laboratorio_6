package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DeploymentScript {

    public static SQLiteDatabase RunScript(Context context) {
        //clearDatabase(context);
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        dataBaseHelper.onCreate(db);
        createPersonas(context);

        return db;
    }

    private static void createPersonas(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        Persona Leo = new Persona("9-0109-0640", "Leonardo Barrientos", "");
        Leo.insert(context);

        Persona Marco = new Persona("1-0175-0330", "Marco Molina", "");
        Marco.insert(context);

        Persona Felipe = new Persona("4-0034-0546", "Felipe Solís", "");
        Felipe.insert(context);

        Persona Andres = new Persona("7-0189-0098", "Andrés Navarrete", "");
        Andres.insert(context);

        Persona Pablo = new Persona("9-0839-0003", "Pablo Castillo", "");
        Pablo.insert(context);
    }
}
