package cr.ac.ucr.ecci.cql.mipatrones;

public interface MainActivityPresenter {

    // resumir
    void onResume();

    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(Persona persona);

    // destruir
    void onDestroy();

}
