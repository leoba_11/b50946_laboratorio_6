package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

public interface GetListItemsInteractor {

    interface OnFinishedListener {
        void onFinished(List<Persona> items);
    }
    void getItems(OnFinishedListener listener);
}
