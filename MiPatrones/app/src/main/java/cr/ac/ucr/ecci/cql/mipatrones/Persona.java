package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Persona implements Parcelable {

    private String identificacion;
    private String nombre;
    private String idImagen;

    public Persona() {

    }

    public Persona(String identificacion, String nombre, String idImagen) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.idImagen = idImagen;
    }

    protected Persona(Parcel in) {
        identificacion = in.readString();
        nombre = in.readString();
        idImagen = in.readString();
    }


    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(String idImagen) {
        this.idImagen = idImagen;
    }

    // Obtener lista de Personas
    public List<Persona> getListaDePersonas(Context context){
        List<Persona> list = new ArrayList<>();

        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA + ";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(new Persona(cursor.getString(0), cursor.getString(1), cursor.getString(2)
            ));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    // insertar una persona en la base de datos
    public void insert(Context context) {

        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        // Crear un mapa de valores donde las columnas son las llaves
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry._ID, getIdentificacion());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE, getNombre());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_IDIMAGEN, getIdImagen());

        // Insertar la nueva fila
        db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, null, values);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        dest.writeString(idImagen);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @Override
    public String toString() {
        return this.nombre;
    }
}
