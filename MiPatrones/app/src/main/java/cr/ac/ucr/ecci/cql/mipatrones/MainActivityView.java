package cr.ac.ucr.ecci.cql.mipatrones;

// Capa de presentacion (Vista)
// Implementado por MainActivity


import java.util.List;

public interface MainActivityView  {
// Mostrar el progreso en la UI del avance de la tarea a realizar

    void showProgress();

    // Esconder el indicador de progreso de la UI
    void hideProgress();

    // Mostrar los items de la lista en la UI
    void setItems(List<Persona> items);

    // Mostrar mensaje en la UI
    void showMessage(String message);

    void setFragment(Persona persona);
}
