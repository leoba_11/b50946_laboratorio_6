package cr.ac.ucr.ecci.cql.mipatrones;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements
        // interface View (V) para implementar los metodos de UI
        MainActivityView,
        // interface para implementar el listener del metodo onItemClick de la lista
        AdapterView.OnItemClickListener {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;

    private static Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DeploymentScript.RunScript(getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mProgressBar = findViewById(R.id.progress);

        // Llamada al Presenter
        mMainActivityPresenter =  new MainActivityPresenterImpl(this);

        myContext = getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }

    // Esconder el indicador de progreso de la UI
    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }

    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override
    public void setItems(List<Persona> items) {
        LazyAdapter mAdapter = new LazyAdapter(items, this);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void setFragment(Persona persona) {

        Bundle bundle = new Bundle();
        PersonaFragment detalles = new PersonaFragment();
        bundle.putParcelable("currentItem", persona);
        detalles.setArguments(bundle);
        FragmentTransaction fragTrac = getFragmentManager().beginTransaction();
        fragTrac.replace(R.id.persona_detalles, detalles);
        fragTrac.commit();
    }

    // Mostrar mensaje en la UI
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // Evento al dar clic en la lista
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Persona item = (Persona) mListView.getAdapter().getItem(position);
        mMainActivityPresenter.onItemClicked(item);
    }

    public static Context getContext() {
        return myContext;
    }
}

