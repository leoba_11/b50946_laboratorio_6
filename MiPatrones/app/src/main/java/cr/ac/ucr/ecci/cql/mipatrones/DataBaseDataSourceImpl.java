package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource {

    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException {

        List<Persona> items = null;
        try {
            // TODO: Obtener de la base de datos
            items = CreateArrayList(MainActivity.getContext());

        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }

        return items;
    }


    private List<Persona> CreateArrayList(Context context) {

        Persona person = new Persona();

        ArrayList<Persona> personas = new ArrayList<>();
        for ( Persona p: person.getListaDePersonas(context) ) {
            personas.add(p);
        }
        return personas.subList(0, personas.size());

    }

}
