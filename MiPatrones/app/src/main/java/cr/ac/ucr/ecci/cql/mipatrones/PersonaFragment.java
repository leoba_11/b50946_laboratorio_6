package cr.ac.ucr.ecci.cql.mipatrones;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import android.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PersonaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonaFragment extends Fragment {

    private Persona item;

    public PersonaFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public static PersonaFragment newInstance(int index) {
        PersonaFragment f = new PersonaFragment();
        // Provee el index como argumento para mostrar el detalle de datos.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if(bundle != null){
            this.item = bundle.getParcelable("currentItem");
        }
        return inflater.inflate(R.layout.fragmento_persona, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.setTextValues(view);
    }

    public void setTextValues(View view){
        TextView nombre = view.findViewById(R.id.nombre);
        nombre.setText(item.getNombre());

        TextView identificacion = view.findViewById(R.id.ID);
        identificacion.setText(item.getIdentificacion());

    }
}
