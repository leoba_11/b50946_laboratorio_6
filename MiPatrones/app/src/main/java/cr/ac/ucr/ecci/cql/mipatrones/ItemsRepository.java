package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

public interface ItemsRepository {

    List<Persona> obtainItems()
            throws CantRetrieveItemsException;
}
